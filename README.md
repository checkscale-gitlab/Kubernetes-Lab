# Basic commands
- `kubectl get nodes`
- `kubectl cluster-info`
- `kubectl run hello-minikube` : Run a pod
- `kubectl apply -f <definitionFile>` : Create & run objects from a definition file
- `kubectl get all` : List all created objects
- `kubectl get pods --all-namespaces` - List all pods in all namespaces
- `kubectl exec ubuntu-sleeper-pod -- ps -aux` - To execute a command on container. we are getting the processes on: the container.

# yaml basic structure
```
apiVersion: [ v1 | apps/v1 ]
kind: [ Pod | ReplicaSet | Service | Deployment ]
metadata: (data about the object)
    Value (name is string type)
    labels: (Dictionary type)
        Key : Value
        Key : Value
spec: `(Specifications of the object)` 
    containers: `(List/Array)`
    - name: `nginx-container`
      image: `nginx`
```
<font color=teal>Note: - name; '-'&nbsp;indicates the index in the list</font>
`kubectl create -f pod-definition.yaml` : creates the pod using yaml file
# PODS
- `kubectl run nginx --image nginx`
- `kubect get pods`
- `kubectl describe pod <pod name>`
- `kubectl delete pod <pod name>`
- `kubectl edit pod <pod name>`
- `kubectl get pod <pod name> -o yaml > pod-definition.yaml`

# Controllers (Replication controller)
- `kubectl get replicationcontroller`
- `kubectl get rc`
- Replication controller helps to run minumum number of pods as specified at all times. 
- If just the pod is created as above, if the pod fails then pod is not replaced.
- required to balance the load on the pod
- Spans across pods and also nodes.

## Replicaset vs Replication controller
- Replication controller is **OLD**
- Replica Set is **NEW**
- Sticking to replica set in this learning.

## rc-definition.yaml file (Replication Controller)
```
apiVersion: v1
kind: ReplicationController
metadata:
    name: myapp-rc
    labels:
        app : myapp
        type : frontend
spec:
    replicas: 2
    template:
        metadata:
            name: `myapp-pod` \
            labels:
                app : `myapp` \
                type : `frontend`
        spec: \
        containers:
        - name: `nginx-container` \
          image: `nginx`
```

# Controllers (ReplicaSet)
- `kubectl get replicaset` \
- `kubectl get rs`
- `kubectl delete replicaset <replicaset name>`
## What is replicaset
- Looks similar to replication controller
- Replica set requires a **selector definition**.
- selector definition helps relicaset identify what pods fall under it. 
- **WHY :** Replicaset can manage pods that were not created as part of the replica set creation.
- Uses matching the labels.

### **rc-definition.yaml file (Replicaset Controller)**
apiVersion: `apps/v1` \
kind: `ReplicaSet` \
metadata: \
&nbsp;&nbsp;&nbsp;name: `myapp-rc` \
&nbsp;&nbsp;&nbsp;labels: \
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;app : `myapp` \
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;type : `frontend`\
spec: \
&nbsp;&nbsp;&nbsp;replicas: `2`\
&nbsp;&nbsp;&nbsp;selector: \
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;matchLabels: \
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;type: `frontend`\
&nbsp;&nbsp;&nbsp;template:\
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;metadata: \
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;name: `myapp-pod` \
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;labels: \
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;app : `myapp` \
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;type : `frontend`
 \
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;spec: \
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;containers: `(List/Array)` \
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;- name: `nginx-container` \
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  image: `nginx`

# Scaling replicas
- `kubectl replace -f <rc-definition.yaml>`
- `kubectl scale --replicas=6 -f rc-definition.yaml`
- `kubectl scale --replicas=6 replicaset <replicaset name>`

# Deployments
- `kubectl get deployments`
- `kubectl delete deployment <depl name>`
- `kubectl create deployment httpd-frontend --image httpd:2.4-alpine && kubectl scale --replicas=3 deployment httpd-frontend`
- Deployment provides the capabilities like *upgrade seemlessly, rolling updates, reverts, pause and resume*
- Deployment ex : replicaset with 6 sets of pods. 
    - Deployment provides all features to the replicaset inturn to the 6 pods running inside the replicaset.
- <font color=red>**Creating a deployment automatically created a new replicaset**</font>

## Deployment-definition.yaml
- Replace *ReplicaSet* with *Deployment*

apiVersion: `apps/v1` \
kind: `Deployment` \
metadata: \
&nbsp;&nbsp;&nbsp;name: `myapp-rc` \
&nbsp;&nbsp;&nbsp;labels: \
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;app : `myapp` \
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;type : `frontend`\
spec: \
&nbsp;&nbsp;&nbsp;replicas: `2`\
&nbsp;&nbsp;&nbsp;selector: \
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;matchLabels: \
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;type: `frontend`\
&nbsp;&nbsp;&nbsp;template:\
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;metadata: \
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;name: `myapp-pod` \
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;labels: \
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;app : `myapp` \
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;type : `frontend`
 \
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;spec: \
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;containers: `(List/Array)` \
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;- name: `nginx-container` \
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  image: `nginx`

# NameSpaces
- `kubectl get pods -n <namespace>`
- `kubectl create -f deployment.yaml --namespace=dev`
- `kubectl create -f deployment.yaml --n dev`
- Mention **namespace: dev** in definition file **metadata**
- Isolates objects such as pods, deployments and Services
- *Default, kube-system, kube-public* are by default available with kubernetes
- Each set of namespaces can have its own *namespaces, resources* Quota (limit)

## Pointing to a service in a default namespace
mysql.connect("db-service")

## Pointing to a service in a default namespace
mysql.connect("db-service.dev.svc.cluster.local")
- When a nameapace is created, a DNS entry is added automatically.
    - cluster.local - domain
    - svc - sub domain
    - dev - namespace
    - db-service - service name

## Managing Namespace
apiVersion: `v1`\
kind: `NameSpace`\
metadata:\
&nbsp;&nbsp;&nbsp;name: `dev`

`kubectl create namsespace <name of namespace>`

## Switch Namespace
`kubectl config set-context $(kubectl config current-context) --namespace=dev`
- `kubectl get pods --all-namespaces` - List all pods in all namespaces

## NameSpace Quota
- Create ResourceQuota for a namespace

apiVersion: `v1`\
kind: `ResourceQuota`\
metadata:\
&nbsp;&nbsp;&nbsp;name: `compute-Quota` \
&nbsp;&nbsp;&nbsp;namespace: `dev` \
spec: \
&nbsp;&nbsp;&nbsp;hard:\
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;pods: `"10"` \
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;requests.cpu: `"4"` \
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;requests.memory: `5Gi`\
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;limits.cpu: `"10"`\
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;limits.memory: `10Gi`

# SERVICES
- Used to manage network among pods
- Used to manage exposure of pods to outer network

## Node Port
A NodePort is an open port on every node of your cluster. Kubernetes transparently routes incoming traffic on the NodePort to your service, even if your application is running on a different node. However, a NodePort is assigned from a pool of cluster-configured NodePort ranges (typically 30000–32767)

```yaml
---
apiVersion: v1
kind: Service
metadata:
  name: webapp-service
spec:
  type: NodePort
  ports:
    - targetPort: 8080
      port: 8080
      nodePort: 30080
  selector:
    name: simple-webapp
```
## Cluster IP
- Only to expose network among pods
- Scaling is easy because automatically network is added to pods
- ClusterIP is the default type (no need to specify)
![Cluster IP](./Media/clusterip.png)

## Definition file
```yaml
apiVersion: v1
kind: Service
metadata:
    name: back-end (name of the service)
spec:
    type: ClusterIP
    ports:
        - targetPort: 80    (where backend is exposed)
          port: 80          (where service is exposed)
    selector:
        app: myapp
        type: back-end
```
## Commands
kubectl create -f service.yaml
kubectl get service or svc

---
## Load Balancer
- type: LoadBalancer


# Labels and Selectors
To **group** and **filter** kubernetes objects. ex, filter and group animals based on color, kind, type, etc...
![](./Media/labels.png)
- Each object can be attached with labels and then filtered

## specifying labels in kubernetes
```yaml
apiVersion: v1
kind: Pod
metadata:
    name: webapp-pod
    labels:
        app: App1
        function: web-app
    spec:
        containers:
        - name: simple-webapp
          image: simple-webapp
          ports:
            -containerPort: 8080
```
`kubectl get pods --selector app=App1`

## Labels & Selectors for replicasets
```yaml
apiVersion: apps/v1
kind: ReplicaSet
metadata: 
    name: simple-webapp
    labels:   (Labels for the replicaset itself)
        app: App1
        function: Front-end
spec: 
    replicas: 3
    selector:
        matchLabels:
            app: App1 (Connects replicaset to pod)
    template:
        metadata:
            labels: (Labels for the )
                app: App1
                function: Front-end
        spec:
            containers:
            - name: simple-webapp
                image: simple-webapp
```
### Selectors in Service definition
```yaml
---
apiVersion: v1
kind: Service
metadata:
    name: my-service
spec:
    selector:
        app: App1
    ports:
    - protocol: TCP
      port: 80
      targetPort: 9376
```

`kubectl get pods --selector env=dev`\
`kubectl get pods --selector bu=finance`\
`kubectl get pods --selector env=prod`\
`kubectl get all --selector env=prod`\
`kubectl get pods --selector env=prod bu=finance`\
`kubectl get pods --selector env=prod,bu=finance,tier=frontend`

# SCHEDULING
Scheduling is used to manually decide where the container runs. You can decide the node manually
- use nodeName: \<node name>
- Code at : [Here](./Code/Scheduling.yaml)

```yaml
---
apiVersion: v1
kind: Pod
metadata:
  name: nginx
spec:
  containers:
  -  image: nginx
     name: nginx
  nodeName: controlplane
```

<<<<<<< HEAD


=======
>>>>>>> 129e3ffb2b691e19e6d7ad8975749457b48c9d43
# ENVIRONMENT VARIABLE
Used to pass environment variables to the pods
ex
```yaml
apiVersion: v1
kind: pod
metadata:
    simple-webapp-color
spec:
    containers:
    - name: simple-webapp-color
      image: simple-webapp-color
      ports:
        - containerPort: 8080
      env:
        - name: APP_COLOR
          value: Red
```

- Other ways : Config maps and Secrets
1. ConfigMaps
```yaml
apiVersion: v1
kind: pod
metadata:
    simple-webapp-color
spec:
    containers:
    - name: simple-webapp-color
      image: simple-webapp-color
      ports:
        - containerPort: 8080
      env:
        - name: APP_COLOR
          valueFrom:
            configMapKeyRef: 
```
2. Secrets
```yaml
apiVersion: v1
kind: pod
metadata:
    simple-webapp-color
spec:
    containers:
    - name: simple-webapp-color
      image: simple-webapp-color
      ports:
        - containerPort: 8080
      env:
        - name: APP_COLOR
          valueFrom:
            secretKeyRef: 
```

# ConfigMaps
Used to manage env variables 
- central management of env variables
- Key Value pairs
- Inject map to yaml

Step 1: Create Config Map (Imperative)
```
kubectl create configmap \
    <config-name> --from-literal=<key>=<value>

kubectl create configmap \
    app-config --from-literal=APP_COLOR=blue \
               -- from-literal=APP_PORT=2000
```
Do it from file
```
kubectl create configmap \
    app-config --from-file=app.properties
```

Create Config Map (Declarative)
```yaml
apiVersion: v1
kind: ConfigMap
metadata:
    name: app-config
data:
    APP_COLOR: blue
    APP_PORT: 2000
```
```kubectl create -f configmap.yaml``` \
```kubectl get configmaps``` \
```kubectl describe configmaps```\

Step 2: Inject Configmap
```yaml
apiVersion: v1
kind: pod
metadata:
    simple-webapp-color
spec:
    containers:
    - name: simple-webapp-color
      image: simple-webapp-color
      ports:
        - containerPort: 8080
      envFrom:
        - configMapKeyRef:
            name: app-config
```

3 ways of injecting Config maps \
1. ENV
```yaml
---
apiVersion: v1
kind: Pod
metadata:
  labels:
    name: webapp-color
  name: webapp-color
  namespace: default
spec:
  containers:
  - image: kodekloud/webapp-color
    name: webapp-color
    envFrom:
     - configMapRef:
         name: webapp-config-map
```
2. SINGLE ENV
```yaml
env:
    - name: APP_COLOR
      valueFrom:
        configMapRef:
            name: app-config
            key: APP_COLOR
```
3. VOLUME
```yaml
volumes:
- name: app-config-volume
  configMap:
    name: app-config
```

# Secrets
To store the credentials and share. 2 step process
1. Create secret
2. Inject secret to pod

Step 1: Create secret(Imperative)
```
kubectl createsecret generic \
    app-secret --from-literal=DB_HOST=root
```
```
kubectl createsecret generic \
    app-secret --from-file=app_secret.properties
```

Create secret(Declarative)
```yaml
apiVerson: v1
kind: Secret
metadata:
    name: app-secret
data:
    DB_HOST: mysql
    DB_USER: root
    DB_PASSEORD: password
```
But doing this is problematic. Hence hash it using
```
echo -n 'mysql' | base64
o/p: ccc
```
Replace in the file.
```yaml
apiVerson: v1
kind: Secret
metadata:
    name: app-secret
data:
    DB_HOST: bX1zcWw=
    DB_USER: bX1zcWw=asdf
    DB_PASSEORD: pabX1zcWw=asdfssword
```
```
kubectl get secrets
```
```
kubectl describe secrets
```
```
kubectl get secret app-secret -o yaml
```
Output of above command is in hash
```
echo -n 'bX1zcWw=' | base64 --decode
```

Step2 : Inject to pod
```yaml
apiVersion: v1
kind: Pod
metadata:
    name: simple-webapp-color
    labels:
       name: simple-webapp-color
spec:
    containers:
    - name: simple-webapp-color
      image: simple-webapp-color
      ports:
        - containerPort: 8080
      envFrom:
        - secretRef:
            name: app-secret
```

Other ways to inject secrets to pods
```yaml
envFrom:
    - secretRef:
        name: app-config
```
```yaml
env:
    - name: DB_PASSWORD
      valueFrom:
        secretKeyRef: 
```
```yaml
volumes:
- name: app-secret-volume
  secret:
    secretName: app-secret
```
While mounting secrets as volumes, each KV has to be in a separate file.

# Security Contexts
## Security in Docker
- All the containers of docker run on a different namespace
- Deployed app containers run in different namespace but uses the unix kernel.
- By default containers are run as root. 
- You can set the username from docker run command. 
    ```
    docker run --user=1000 ubuntu sleep 3600
    ```
    Docker file:
    ```
    FROM ubuntu
    USER 1000
    ```

ROOT USER
- root user of the host is not similar to the root user running as the container
- docker uses linux capabilities to reduce the power of container root user. 
- docker run with limited capabilities. 
- Add capabilities
    ```
    docker run --cap-add MAC_ADMIN ubuntu
    docker run --cao-drop KILL ubuntu
    ```
    To enable all privileges
    ```
    docker run --privileged ubuntu
    ```


## Security contexts in Kubernetes
- You can configure security to either pod level or container level
- With pod level security, applies to all containers
- With pod and container level, container level security overrides pod level security settings.

Adding security to `container` level
```yaml
apiVersion: v1
kind: Pod
metadata: 
    name: web-pod
spec:
    securityContext:
        runAsUser: 1000
    containers:
        - name: ubuntu
          image: ubuntu
          command: ["sleep", "3600"]
```

Adding security to pod level
```yaml
apiVersion: v1
kind: Pod
metadata: 
    name: web-pod
spec:
    containers:
        - name: ubuntu
          image: ubuntu
          command: ["sleep", "3600"]
          securityContext:
                runAsUser: 1000
```

Adding capabilities to pod level
```yaml
apiVersion: v1
kind: Pod
metadata: 
    name: web-pod
spec:
    containers:
        - name: ubuntu
          image: ubuntu
          command: ["sleep", "3600"]
          securityContext:
                runAsUser: 1000
          capabilities:
                add: ["MAC_ADMIN"]
```

NOTE: capabilities are supported to only container level and not the pod level

<<<<<<< HEAD
# Resource Re quirements
Limiting resources per pod
- Scheduler consider the resources required and assigns to node.
- if node is full, and there are no nodes, shceduler holds pod. and pod stays in pending state
- By default : 512Mi mem and 1vCPU is set

Ex:
cpu : 0.5
MEM : 256 Mi
Dsk : 
```
resources:
    requests:
        memory: "1G" | "1024M" | 1Gi | 1Mi
        cpu: 1
```
1 cpu means 1 hyperthread and 1 vcpu in aws

To override default values
```
resources:
    requires:
        memory: "1Gi"
        cpu: 1
    limits:
        memory: "2Gi"
        cop: 2
```
requires sets the minimum and limits set the max.

NOTE: Resources are set at container level in each pod.

Throttle : If more than specified cpus are consumed, then K8s throttles so that more cpus are not consumed
Terminate : If more than given memory is consumed, then k8s terminate the pod

# Service Accounts
part of K8s security 

Two types of accounts
- User account
- Service account

Service account is used by application to interact with K8s. Ex Prometheus, Jenkins, etc...

```
kubectl create serviceaccount dashboard-sa
```
```
kubectl get serviceaccount
```
```
kubectl describe serviceaccount dashboard-sa
```
service accounts creates a secret object in which secret holds the token. And that secret is attached to the service account.

To access the token
```
kubectl describe secret dashboard-sa-token-kbbm
```

Use the token with curl
```
curl https://192.168.1.1:6443/api -insecure --header "Authorization: Bearer <TOKEN>"
```

if the 3rd party application is deployed on the k8s cluster, then exporting token is not necessary. Just mount secret as volume in the pod with 3rd party application.

- Every namespace has a default service account created. 
- Every new pod will have default secret mounter.

Add service account in the definition file
```
apiVersion: v1
kind: Pod
metadata:
    name: dashboard-app
spec:
    containers:
        - name: dashboard-app
          image: dashboard-app
    serviceAccountName: dashboard-sa
```

NOTE: Editing pod is not possible for serviceAccountName.


Disable default service account mounting^
```
apiVersion: v1
kind: Pod
metadata:
    name: dashboard-app
spec:
    containers:
        - name: dashboard-app
          image: dashboard-app
    serviceAccountName: dashboard-sa
    automountServiceAccountToken: false
```

# Taints and Tolerations
|Command|Description|
|---|---|
|kubectl taint nodes node01 app=blue:taint-effect| Dedicate node01 to applications blue |

## Taint Effects
What happens if pods do not tolerate the taint
|Taint|Effect|
|---|---|
|NoSchedule | Pod that cannot tolerate are not scheduled|
|PreferNoSchedule | pod that cannot tolerate are preferably not scheduled|
|NoExecute | No new pods are scheduled if not tolerated and also existing pods are evicted if not tolerated |

## Example
kubectl taint nodes node01 app=blue :NoSchedule\
Taint node with app=blue and no schedule as taint effect

To apply toleration
```
apiVersion: v1
kind: Pod
metadata:
    name: myapp-pod
spec:
    containers:
    - name: nginx-container
      image: nginx
    tolerations:
    - key:
      operator: "Equal"
      value: "blue"
      effect: "NoSchedule"    
```

=======


## Resource Requirements
To limit resource limits on the pods. if node is full, insufficient CPU or memory is logged and pod stays in pending stays.

[Resource requirements.yaml](resource_requirements.yaml)
```yaml
apiVersion: v1
kind: Pod
metadata:
  name: webapp
  labels:
    name: webapp
spec:
  containers:
    - name: webapp
      image: sample-webapp-color
      ports:
        - containerPort: 8080
      resources:
        requests:
          memory: "1Gi"
          cpu: 1
        limits:
          memory: "128Mi"
          cpu: "500m"
```

- limits are set to each container.
- CPU : kubernetes throttles to limit cpu to go beyond limit
- Memory : container can use more memory than limit. if pod consume more memory CONSTANTLY, its terminated.

# Service Accounts
2 types of accounts
- user account : by uers > admins, developers, etc
- service account : by machine > apps, prometheus, grafana, jenkins, etc.

## Commands
| command | Description |
| --- | --- |
| `kubectl create serviceaccount dashboard-sa` | Create a new service account |
| `kubectl get serviceaccount` | List all service accounts |
| `kubectl describe secret name` | Describe a secret |
| `kubectl describe secret <name>` | To read the token associated with the service account |

## Code
```yaml

```


## Additional Information
Creation
- Creating a service account by default creates a token which is by default attached to the service account.
- Token is basically stored in a secret.
- Then that secret is attached to the service account

Default service account
- Every name space has a default service account
- Default sa and token are **mounted as VOLUME** on a pod for intra security management
Commabds to get mount and secret information
  | command | Description |
  | --- | --- |
  | `kubectl describe pod <pod name>` | Just look in the mounts config to find secret information |
  | `kubectl exec -it <pod> ls /var/run/secrets/kubernetes.io/serviceaccount` | List files in the folder where cred files are present |
  | `kubectl exec -it <pod> ls /var/run/secrets/kubernetes.io/serviceaccount/token` | print token file |

  Attaching a service account to pod
  ```yaml
  apiVersion: v1
  kind: Pod
  metadata:
    name: dashboard
  spec:
    containers:
      - name: dashboard
        image: dashboard-image
    serviceAccountName: dashboard-sa
    automountServuceAccountToken: false
  ```
- You cannot edit Pod while changing service account. 
- However its possible in case of deployments

## Links
[k8s.io Secrets](https://kubernetes.io/docs/tasks/configure-pod-container/configure-service-account/)


# Taints and Tolerations
Taints are used to restrict nodes to limit what pods should should be deployed on to it.
| command | Description |
| --- | --- |
| Taint | Applied on nodes to restrict pods |
| Toleratiions | Applied to pods so that they can be deployed on to tainted nodes |

## Commands
| command | Description |
| --- | --- |
| `kubectl taint nodes node-name key=value:taint-effect` | To taint a node |

## Additional Information
- Taints are applied on nodes, tolerations are applied on pods

Taint-Effects
| Taint effect | Description |
| --- | --- |
| NoSchedule | Pods are not scheduled on the node |
| PreferNoSchedule | K8s tries not to schedule |
| NoExecute | New pods are not scheduled and existing pods(non tainted) will be evicted |
## Code to apply toleration to a pod
Code for the taint on a node as below
```
kubectl taint nodes node1 app=blue:NoSchedule
```
```yaml
apiVersion: v1
kind: Pod
metadata: 
  name: dashboard
spec:
  containers:
    - name: dashboard
      image: dashboard
  tolerations:
    - key: "app"
      operator: "Equal"
      value: "blue"
      effect: "NoSchedule"
```
Note: 
- All toleration data has to be encoded in **Double-Quotes**
- It is not a mandate that tolerated pod lands on exactly the tainted node. It may be scheduled on other nodes
- To define where the pod has to be scheduled, the next topic is used #[Node Affinity](#NodeAffinity)


## Links
[k8s.io](https://kubernetes.io/docs/concepts/scheduling-eviction/taint-and-toleration/)

# NodeAffinity
To fix which pod has to go on to which node.
- Toleration and taints doesnot gurantee node placements
- Affinity helps in assigning fixed nodes
  - First taint the node with names
  - Next using selectors while creating the pods.
  - Then apply the tolerations on other pods so that they can also be scheduled.
Thus a combination of taints, tolerations and affinity can be used. 

## Commands

| command | Description |
| --- | --- |
| `TopicName` |  |


## Additional Information


## Code
### 1. Applying node selector
```yaml
apiVersion: v1
kind: Pod
metadata: 
  name: dashboard
  labels:
    app: dashboard
spec:
  containers:
    - name: dashboard
      image: dashboard
  nodeSelector:
    size: Large
```
### 2. Same using node affinity
```yaml
apiVersion: v1
kind: Pod
metadata: 
  name: dashboard
  labels:
    app: dashboard
spec:
  containers:
    - name: dashboard
      image: dashboard
  affinity:
    nodeAffinity:
      requiredDuringSchedulingIgnoreDuringExecution:
      - matchExpressions:
        - key: size
          operator: In | NotIn | Exists
          values:
          - Large
          - Medium
```
| Type | Description |
| --- | --- |
| requiredDuringSchedulingIgnoredDuringExecution | When pod is created for the first time, affinity rule apply. If a pod is already running its left like that |
| preferredDuringSchedulingIgnoredDuringExecution | If not matching node is not found, places pod on any available node. Same as above |
| requiredDuringSchedulingIgnoredDuringExecution | If not matching node found, its not scheduled. But eviction happens if affinity rules are not matched |
Both does the same thing but affinity provides flexibility to select based on the expression.

## Links
[k8s.io](https://kubernetes.io/docs/concepts/scheduling-eviction/assign-pod-node/)

# Multi-Container Pods

- 
- 

## Commands

| command | Description |
| --- | --- |
| `Multi-Container Pods` |  |


## Additional Information


## Code
```yaml

```


## Links
[Localfile](Path1)\
[k8s.io](Path2)
>>>>>>> 129e3ffb2b691e19e6d7ad8975749457b48c9d43
















